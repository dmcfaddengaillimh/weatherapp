# CORS Issue (September 7th, 2017)

As GitHub Pages does not allow Cross Origin Resource Sharing (CORS), I am unable to pull IP address information for the API calls. This fails to show any data. Screenshots will be uploaded to see what the app looks like with data in it when run locally. I have looked into several options to see if GitHub Pages allows a user to work around this.

# Weather-Forecast-App
First use of an API Key from APIXU for weather data. Displays longititude in the console
